from django.template import Template, context
from django.template.loader import get_template
from django.shortcuts import render
import requests
from EL_MINIMARKET_10.integration.clientws_asunto import find_all_despachos, find_all_retiros, find_all_reclamos, find_all_sugerencias, add_despacho, add_reclamo, add_retiro, add_sugerencia   
from EL_MINIMARKET_10.integration.clientws_asunto import delete_by_id_despacho, delete_by_id_retiro, delete_by_id_reclamo, delete_by_id_sugerencia, update_despacho, update_retiro, update_reclamo, update_sugerencia, ws_login                                                    
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
from django.http import HttpResponse


def authorization(request):
    try:
        token = request.session['token']
        print('token: {0}'.format(request.session['token']))

        if token is None:
            return False
        else: 
            return True
    except Exception as e:
        print('token no encontrado')
        return False

def authentication(request):
    
    username = request.POST.get("username")
    password = request.POST.get("password")
    print('username: {0}'.format(username))
    print('password: {0}'.format(password))

    token = ws_login(username, password)
    print('token: {0}'.format(password))
    request.session['token'] = token
    
    return redirect('detalle-form-contact')

def login(request):
    print('login')
    return render(request, 'login.html')
  
def home(request):
    try:
        print('token: {0}'.format(request.session['token']))
    except Exception as e:
        print('token no encontrado')

    DIA = ''
    FEACHA = ''
    TEMPMAX = ''
    TEMPMIN = ''
    try:
        url = 'http://api.meteored.cl/index.php?api_lang=cl&localidad=18578&affiliate_id=zh98se47jpam&v=3.0'
        response = requests.get(url)
        
        data = response.json()
        day = data['day']
        print('')
        print('dia: {0}'.format(day['1']['name']))
        print('fecha: {0}'.format(day['1']['date']))
        print('temperatura max: {0}'.format(day['1']['tempmax'])+'°')
        print('temperatura min: {0}'.format(day['1']['tempmin'])+'°')
        DIA = day['1']['name']
        FEACHA = day['1']['date']
        TEMPMAX = day['1']['tempmax']
        TEMPMIN = day['1']['tempmin']
    except:
        print("fallo")
    return render(request, 'index.html',{'DIA': DIA, 'FEACHA': FEACHA, 'TEMPMAX': TEMPMAX, 'TEMPMIN': TEMPMIN})

def gallery(request):
    return render(request, 'gallery.html')

def gallerycard(request):
    return render(request, 'gallery-card.html')    

def contact(request):
    return render(request, 'contact.html')


def agregar_asunto(request):


    print("INIT AGREGAR ASUNTO")
    if request.method == "POST":

        print("INIT AGREGAR ASUNTO")
        if request.POST['opcion'] == "1":
            add_despacho(request)
        elif request.POST['opcion'] == "2":
            add_retiro(request)
        elif request.POST['opcion'] == "3":
            add_sugerencia(request)
        elif request.POST['opcion'] == "4":
            add_reclamo(request)        
    return render(request, 'contact.html')

def detalle_form_contact(request):
    if not authorization(request): 
        return render(request, 'login.html')
    return render(request, 'detalle-form-contact.html')

def eliminar_despacho(request):
    
    if not authorization(request): 
        return redirect("login")
    print("INIT ELIMINAR ASUNTO")
    
    if request.method == "GET":
        token = request.session['token']
        delete_by_id_despacho(request.GET['id'], token)
        delete_by_id_retiro(request.GET['id'], token)
        delete_by_id_reclamo(request.GET['id'], token)
        delete_by_id_sugerencia(request.GET['id'], token)
        return render(request, 'detalle-form-contact.html')  
        
                      

def editar_asunto(request):
    if not authorization(request): 
        return redirect("login")
    
    print("INIT EDITAR ASUNTO")
    if request.method == "POST":
        token = request.session['token']
        print(request.POST)
        update_despacho(request, token)
        update_retiro(request, token)
        update_reclamo(request, token)
        update_sugerencia(request, token)         
    print("FINISH EDITAR ASUNTO")
    return render(request, 'detalle-form-contact.html') 


##cargar actualizado
def cargar(request):
    if not authorization(request): 
        return render(request, 'login.html', {"msg_error": 'USUARIO O CONTRASEÑAS INVALIDAS'})
    if request.method == "POST":
        cargar = request.POST['cargar']
        print("opcion: {0}".format(cargar)) 

        try:
            token = request.session['token']
            if cargar == "1":
                tablas=find_all_despachos(token);
            elif cargar == "2":
                tablas=find_all_retiros(token);
            elif cargar == "3":
                tablas=find_all_sugerencias(token);
            elif cargar == "4":
                tablas=find_all_reclamos(token);

        
            return render(request, 'detalle-form-contact.html',{'tablas': tablas}) 
        except Exception as e:
            print (e)
            return render(request, 'login.html', {"msg_error": 'USUARIO O CONTRASEÑAS INVALIDAS'})
