import requests
from django.http import HttpResponse
import json

def ws_login(username, password):
    print('login')
    url = 'http://127.0.0.1:9000/api/v1/login'
    try:
        json_authentication = {'username': username, 'pass': password}
        response = requests.post(url, json=json_authentication)
        response.encoding = 'utf-8' 
        print('status: {0}'.format(response.status_code))

        if response.status_code == 200 :
            token_json = response.json()
            print('token_json: {0}'.format(token_json))
            return token_json.get('token')
        else:
            print('message: {0}'.format(response.text))
            print('Error al autentincar')

    except Exception as e:
        print("ERROR INVOCACIÓN SERVICIO : {0}, {1}".format(url, e)) 

def find_all_despachos(token):
    print('find_all_despacho')
    url = 'http://127.0.0.1:9000/api/v1/despacho'
    headers = {'Authorization': 'Token {0}'.format(token)}
    response = requests.get(url, headers=headers)
    response.encoding = 'utf-8'
    print('response: {0}'.format(response))
    print('status: {0}'.format(response.status_code))
    if response.status_code == 200:
        print('JSON: {0}'.format(response.json()))
        return response.json()
    else:
        print('message: {0}'.format(response.text))
        raise Exception(message)


def find_all_retiros(token):
    print('find_all_retiro')
    url = 'http://127.0.0.1:9000/api/v1/retiro'
    headers = {'Authorization': 'Token {0}'.format(token)}
    response = requests.get(url, headers=headers)
    response.encoding = 'utf-8'
    print('response: {0}'.format(response))
    print('status: {0}'.format(response.status_code))
    if response.status_code == 200:
        print('JSON: {0}'.format(response.json()))
        return response.json()
    else:
        print('message: {0}'.format(response.text))
        raise Exception(message)

def find_all_reclamos(token):
    print('find_all_reclamo')
    url = 'http://127.0.0.1:9000/api/v1/reclamo'
    headers = {'Authorization': 'Token {0}'.format(token)}
    response = requests.get(url, headers=headers)
    response.encoding = 'utf-8'
    print('response: {0}'.format(response))
    print('status: {0}'.format(response.status_code))
    if response.status_code == 200:
        print('JSON: {0}'.format(response.json()))
        return response.json()
    else:
        print('message: {0}'.format(response.text))
        raise Exception(message)    


def find_all_sugerencias(token):
    print('find_all_sugerencia')
    url = 'http://127.0.0.1:9000/api/v1/sugerencia'
    headers = {'Authorization': 'Token {0}'.format(token)}
    response = requests.get(url, headers=headers)
    response.encoding = 'utf-8'
    print('response: {0}'.format(response))
    print('status: {0}'.format(response.status_code))
    if response.status_code == 200:
        print('JSON: {0}'.format(response.json()))
        return response.json()
    else:
        print('message: {0}'.format(response.text))
        raise Exception(message)                      


def add_despacho(request):
    print('add_asunto')   
    url = 'http://127.0.0.1:9000/api/v1/despacho'
    try:
            run = request.POST['run']
            nombres = request.POST['nombres']
            apellido_paterno = request.POST['apellido-paterno']
            apellido_materno = request.POST['apellido-materno']
            telefono = request.POST['telefono']
            direccion = request.POST['direccion']
            email = request.POST['email']    
            motivo = request.POST['motivo']  

            json = {
                    "motivo": motivo,
                    "email": email,
                    "telefono": telefono,
                    "codigo_cliente": {
                        "run": run,
                        "nombres": nombres,
                        "apellido_paterno": apellido_paterno,
                        "apellido_materno": apellido_materno,
                        "direccion": direccion,                                                                       
                    }
                }

            print(json)
            response = requests.post(url, json=json)
            print('response code: {0}'.format(response.status_code))
            print('response body -> {0}'.format(response.json()))

    except Exception as e:
        print("ERROR INVOCACIÓN SERVICIO : {0}, {1}".format(url, e)) 

def add_reclamo(request):
    print('add_asunto')   
    url = 'http://127.0.0.1:9000/api/v1/reclamo'
    try:
            run = request.POST['run']
            nombres = request.POST['nombres']
            apellido_paterno = request.POST['apellido-paterno']
            apellido_materno = request.POST['apellido-materno']
            telefono = request.POST['telefono']
            direccion = request.POST['direccion']
            email = request.POST['email']    
            motivo = request.POST['motivo']  

            json = {
                    "motivo": motivo,
                    "email": email,
                    "telefono": telefono,
                    "codigo_cliente": {
                        "run": run,
                        "nombres": nombres,
                        "apellido_paterno": apellido_paterno,
                        "apellido_materno": apellido_materno,
                        "direccion": direccion,                                                                       
                    }
                }

            print(json)
            response = requests.post(url, json=json)
            print('response code: {0}'.format(response.status_code))
            print('response body -> {0}'.format(response.json()))

    except Exception as e:
        print("ERROR INVOCACIÓN SERVICIO : {0}, {1}".format(url, e))   

def add_retiro(request):
    print('add_asunto')   
    url = 'http://127.0.0.1:9000/api/v1/retiro'
    try:
            run = request.POST['run']
            nombres = request.POST['nombres']
            apellido_paterno = request.POST['apellido-paterno']
            apellido_materno = request.POST['apellido-materno']
            telefono = request.POST['telefono']
            direccion = request.POST['direccion']
            email = request.POST['email']    
            motivo = request.POST['motivo']  

            json = {
                    "motivo": motivo,
                    "email": email,
                    "telefono": telefono,
                    "codigo_cliente": {
                        "run": run,
                        "nombres": nombres,
                        "apellido_paterno": apellido_paterno,
                        "apellido_materno": apellido_materno,
                        "direccion": direccion,                                                                       
                    }
                }

            print(json)
            response = requests.post(url, json=json)
            print('response code: {0}'.format(response.status_code))
            print('response body -> {0}'.format(response.json()))

    except Exception as e:
        print("ERROR INVOCACIÓN SERVICIO : {0}, {1}".format(url, e))         

def add_sugerencia(request):
    print('add_asunto')   
    url = 'http://127.0.0.1:9000/api/v1/sugerencia'
    try:
            run = request.POST['run']
            nombres = request.POST['nombres']
            apellido_paterno = request.POST['apellido-paterno']
            apellido_materno = request.POST['apellido-materno']
            telefono = request.POST['telefono']
            direccion = request.POST['direccion']
            email = request.POST['email']    
            motivo = request.POST['motivo']  

            json = {
                    "motivo": motivo,
                    "email": email,
                    "telefono": telefono,
                    "codigo_cliente": {
                        "run": run,
                        "nombres": nombres,
                        "apellido_paterno": apellido_paterno,
                        "apellido_materno": apellido_materno,
                        "direccion": direccion,                                                                       
                    }
                }

            print(json)
            response = requests.post(url, json=json)
            print('response code: {0}'.format(response.status_code))
            print('response body -> {0}'.format(response.json()))

    except Exception as e:
        print("ERROR INVOCACIÓN SERVICIO : {0}, {1}".format(url, e))            


def delete_by_id_despacho(id, token):
    print('delete_by_id_despacho')   
    url = 'http://127.0.0.1:9000/api/v1/despacho/{0}'.format(id)
    try:
        headers = {'Authorization': 'Token {0}'.format(token)}
        response = requests.delete(url, headers=headers)
        print('response code: {0}'.format(response.status_code))
        print('response code: {0}'.format(response.text))
    except Exception as e:
        print("ERROR INVOCACIÓN SERVICIO : {0}, {1}".format(url, e))             

def delete_by_id_retiro(id, token):
    print('delete_by_id_retiro')   
    url = 'http://127.0.0.1:9000/api/v1/retiro/{0}'.format(id)
    try:
        headers = {'Authorization': 'Token {0}'.format(token)}
        response = requests.delete(url, headers=headers)
        print('response code: {0}'.format(response.status_code))
        print('response code: {0}'.format(response.text))
    except Exception as e:
        print("ERROR INVOCACIÓN SERVICIO : {0}, {1}".format(url, e))     

def delete_by_id_reclamo(id, token):
    print('delete_by_id_reclamo')   
    url = 'http://127.0.0.1:9000/api/v1/reclamo/{0}'.format(id)
    try:
        headers = {'Authorization': 'Token {0}'.format(token)}
        response = requests.delete(url, headers=headers)
        print('response code: {0}'.format(response.status_code))
        print('response code: {0}'.format(response.text))
    except Exception as e:
        print("ERROR INVOCACIÓN SERVICIO : {0}, {1}".format(url, e))     

def delete_by_id_sugerencia(id, token):
    print('delete_by_id_sugerencia')   
    url = 'http://127.0.0.1:9000/api/v1/sugerencia/{0}'.format(id)
    try:
        headers = {'Authorization': 'Token {0}'.format(token)}
        response = requests.delete(url, headers=headers)
        print('response code: {0}'.format(response.status_code))
        print('response code: {0}'.format(response.text))
    except Exception as e:
        print("ERROR INVOCACIÓN SERVICIO : {0}, {1}".format(url, e)) 

def update_despacho(request, token):
    print('add_asunto')   
    url = 'http://127.0.0.1:9000/api/v1/despacho'
    try:
        headers = {'Authorization': 'Token {0}'.format(token)}
        codigo = request.POST["codigo"]
        run = request.POST['run']
        nombres = request.POST['nombres']
        apellido_paterno = request.POST['apellido-paterno']
        apellido_materno = request.POST['apellido-materno']
        telefono = request.POST['telefono']
        direccion = request.POST['direccion']
        email = request.POST['email']        
        motivo = request.POST['motivo']           
        codigo_cliente = request.POST['codigo_cliente']

        json = {
                "codigo": codigo,
                "motivo": motivo,
                "email": email,
                "telefono": telefono,
                "codigo_cliente": {
                    "run": run,
                    "nombres": nombres,
                    "apellido_paterno": apellido_paterno,
                    "apellido_materno": apellido_materno,
                    "direccion": direccion,
                    "codigo": codigo_cliente,                                                                          
                }
            }

        print(json)
        response = requests.put(url, json=json, headers=headers)
        print('response code: {0}'.format(response.status_code))
        print('response body -> {0}'.format(response.json()))

    except Exception as e:
        print("ERROR INVOCACIÓN SERVICIO : {0}, {1}".format(url, e))                    

def update_retiro(request, token):
    print('add_asunto')   
    url = 'http://127.0.0.1:9000/api/v1/retiro'
    try:
        headers = {'Authorization': 'Token {0}'.format(token)}
        codigo = request.POST["codigo"]
        run = request.POST['run']
        nombres = request.POST['nombres']
        apellido_paterno = request.POST['apellido-paterno']
        apellido_materno = request.POST['apellido-materno']
        telefono = request.POST['telefono']
        direccion = request.POST['direccion']
        email = request.POST['email']        
        motivo = request.POST['motivo']           
        codigo_cliente = request.POST['codigo_cliente']

        json = {
                "codigo": codigo,
                "motivo": motivo,
                "email": email,
                "telefono": telefono,
                "codigo_cliente": {
                    "run": run,
                    "nombres": nombres,
                    "apellido_paterno": apellido_paterno,
                    "apellido_materno": apellido_materno,
                    "direccion": direccion,
                    "codigo": codigo_cliente,                                                                         
                }
            }

        print(json)
        response = requests.put(url, json=json, headers=headers)
        print('response code: {0}'.format(response.status_code))
        print('response body -> {0}'.format(response.json()))

    except Exception as e:
        print("ERROR INVOCACIÓN SERVICIO : {0}, {1}".format(url, e))           

def update_reclamo(request, token):
    print('add_asunto')   
    url = 'http://127.0.0.1:9000/api/v1/reclamo'
    try:
        headers = {'Authorization': 'Token {0}'.format(token)}
        codigo = request.POST["codigo"]
        run = request.POST['run']
        nombres = request.POST['nombres']
        apellido_paterno = request.POST['apellido-paterno']
        apellido_materno = request.POST['apellido-materno']
        telefono = request.POST['telefono']
        direccion = request.POST['direccion']
        email = request.POST['email']        
        motivo = request.POST['motivo']           
        codigo_cliente = request.POST['codigo_cliente'] 

        json = {
                "codigo": codigo,
                "motivo": motivo,
                "email": email,
                "telefono": telefono,
                "codigo_cliente": {
                    "run": run,
                    "nombres": nombres,
                    "apellido_paterno": apellido_paterno,
                    "apellido_materno": apellido_materno,
                    "direccion": direccion,
                    "codigo": codigo_cliente,                                                                      
                }
            }

        print(json)
        response = requests.put(url, json=json, headers=headers)
        print('response code: {0}'.format(response.status_code))
        print('response body -> {0}'.format(response.json()))

    except Exception as e:
        print("ERROR INVOCACIÓN SERVICIO : {0}, {1}".format(url, e))   

def update_sugerencia(request, token):
    print('add_asunto')   
    url = 'http://127.0.0.1:9000/api/v1/sugerencia'
    try:
        headers = {'Authorization': 'Token {0}'.format(token)}
        codigo = request.POST["codigo"]
        run = request.POST['run']
        nombres = request.POST['nombres']
        apellido_paterno = request.POST['apellido-paterno']
        apellido_materno = request.POST['apellido-materno']
        telefono = request.POST['telefono']
        direccion = request.POST['direccion']
        email = request.POST['email']        
        motivo = request.POST['motivo']           
        codigo_cliente = request.POST['codigo_cliente']

        json = {
                "codigo": codigo,
                "motivo": motivo,
                "email": email,
                "telefono": telefono,
                "codigo_cliente": {
                    "run": run,
                    "nombres": nombres,
                    "apellido_paterno": apellido_paterno,
                    "apellido_materno": apellido_materno,
                    "direccion": direccion,
                    "codigo": codigo_cliente,                                                                         
                }
            }

        print(json)
        response = requests.put(url, json=json, headers=headers)
        print('response code: {0}'.format(response.status_code))
        print('response body -> {0}'.format(response.json()))

    except Exception as e:
        print("ERROR INVOCACIÓN SERVICIO : {0}, {1}".format(url, e))   


     