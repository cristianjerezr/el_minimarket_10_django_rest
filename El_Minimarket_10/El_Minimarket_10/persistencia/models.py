
from django.db import models


class Cliente(models.Model):
    codigo = models.AutoField(db_column='CODIGO', primary_key=True)  # Field name made lowercase.
    run = models.CharField(db_column='RUN', max_length=15)  # Field name made lowercase.
    nombres = models.CharField(db_column='NOMBRES', max_length=150)  # Field name made lowercase.
    apellido_paterno = models.CharField(db_column='APELLIDO_PATERNO', max_length=120)  # Field name made lowercase.
    apellido_materno = models.CharField(db_column='APELLIDO_MATERNO', max_length=120)  # Field name made lowercase.
    direccion = models.CharField(db_column='DIRECCION', max_length=120)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'CLIENTE'


class Despacho(models.Model):
    codigo = models.AutoField(db_column='CODIGO', primary_key=True)  # Field name made lowercase.
    motivo = models.CharField(db_column='MOTIVO', max_length=500, blank=True, null=True)  # Field name made lowercase.
    email = models.CharField(db_column='EMAIL', max_length=70, blank=True, null=True)  # Field name made lowercase.
    telefono = models.CharField(db_column='TELEFONO', max_length=15, blank=True, null=True)  # Field name made lowercase.
    codigo_cliente = models.ForeignKey(Cliente, models.DO_NOTHING, db_column='CODIGO_CLIENTE')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'DESPACHO'


class Reclamo(models.Model):
    codigo = models.AutoField(db_column='CODIGO', primary_key=True)  # Field name made lowercase.
    motivo = models.CharField(db_column='MOTIVO', max_length=500, blank=True, null=True)  # Field name made lowercase.
    email = models.CharField(db_column='EMAIL', max_length=70, blank=True, null=True)  # Field name made lowercase.
    telefono = models.CharField(db_column='TELEFONO', max_length=15, blank=True, null=True)  # Field name made lowercase.
    codigo_cliente = models.ForeignKey(Cliente, models.DO_NOTHING, db_column='CODIGO_CLIENTE')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'RECLAMO'


class Retiro(models.Model):
    codigo = models.AutoField(db_column='CODIGO', primary_key=True)  # Field name made lowercase.
    motivo = models.CharField(db_column='MOTIVO', max_length=500, blank=True, null=True)  # Field name made lowercase.
    email = models.CharField(db_column='EMAIL', max_length=70, blank=True, null=True)  # Field name made lowercase.
    telefono = models.CharField(db_column='TELEFONO', max_length=15, blank=True, null=True)  # Field name made lowercase.
    codigo_cliente = models.ForeignKey(Cliente, models.DO_NOTHING, db_column='CODIGO_CLIENTE')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'RETIRO'


class Sugerencia(models.Model):
    codigo = models.AutoField(db_column='CODIGO', primary_key=True)  # Field name made lowercase.
    motivo = models.CharField(db_column='MOTIVO', max_length=500, blank=True, null=True)  # Field name made lowercase.
    email = models.CharField(db_column='EMAIL', max_length=70, blank=True, null=True)  # Field name made lowercase.
    telefono = models.CharField(db_column='TELEFONO', max_length=15, blank=True, null=True)  # Field name made lowercase.
    codigo_cliente = models.ForeignKey(Cliente, models.DO_NOTHING, db_column='CODIGO_CLIENTE')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'SUGERENCIA'


