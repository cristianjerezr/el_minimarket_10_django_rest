function enviar(codigo, nombreFormulario) {
    console.log("codigo: " + codigo);
    console.log("codigo.length: " + codigo.length);
    if (codigo.length === 0){
        console.log("codigo vació ");
    }else{
        codigo = "-" + codigo;
    }
    console.log("codigo: " + codigo);
    //Obtención de la información del formulario
    let nombres = document.getElementById("txt-nombres" + codigo).value;
    let apellidoPaterno = document.getElementById("txt-apellido-paterno" + codigo).value;
    let apellidoMaterno = document.getElementById("txt-apellido-materno" + codigo).value;
    let run = document.getElementById("txt-run" +codigo).value;
    let telefono = document.getElementById("txt-telefono" + codigo).value;
    let direccion = document.getElementById("txt-direccion" + codigo).value;
    let email = document.getElementById("txt-email" + codigo).value;
    let opcion = document.getElementById("cmb-opcion" +codigo).value;
    let motivo = document.getElementById("txt-motivo" +codigo).value;
    let mensaje_error = isEmpty(nombres, "NOMBRES");
    //Validación de campos del formulario
    mensaje_error = mensaje_error + isEmpty(apellidoPaterno, "APELLIDO PATERNO");
    mensaje_error = mensaje_error + isEmpty(apellidoMaterno, "APELLIDO MATERNO");
    mensaje_error = mensaje_error + isEmpty(run, "RUN");
    mensaje_error = mensaje_error + isEmpty(telefono, "TELEFONO");
    mensaje_error = mensaje_error + isEmpty(direccion, "DIRECCION");
    mensaje_error = mensaje_error + isEmpty(email, "EMAIL");
    mensaje_error = mensaje_error + isEmpty(opcion, "OPCION");
    mensaje_error = mensaje_error + isEmpty(motivo, "MOTIVO");
    //Mostrar Error
    let msgValidacion = document.getElementById("msg-validacion");
    let msgInformacion = document.getElementById("msg-informacion");
    if (mensaje_error == 0) {
        console.log("DATOS VALIDADOS CORRECTAMENTE.");
        msgValidacion.className = "alert alert-success alert-dismissible fade show";
        msgInformacion.innerHTML = "DATOS VALIDADOS CORRECTAMENTE.";
        document.getElementById(nombreFormulario).submit();
        clearForm(false);
    } else {
        console.log(mensaje_error);
        msgValidacion.className = "alert alert-danger alert-dismissible fade show";
        msgInformacion.innerHTML = mensaje_error.replaceAll("\n", "<br>");
        msgValidacion.hidden = false;
    }
}

function clearForm(msgvalidacionHidden) {
    console.log("INICIO CLEAR FORM");
    clear("txt-nombres");
    clear("txt-apellido-paterno");
    clear("txt-apellido-materno");
    clear("txt-run");
    clear("txt-telefono");
    clear("txt-direccion");
    clear("txt-email");
    clear("cmb-opcion");
    clear("txt-motivo");
    let msgValidacion = document.getElementById("msg-validacion");
    msgValidacion.hidden = msgvalidacionHidden;
    console.log("INICIO CLEAR FORM");
}

function cargando(codigo, nombreFormulario) {
    console.log("codigo: " + codigo);
    console.log("codigo.length: " + codigo.length);

    if (codigo.length === 0){
        console.log("codigo vació ");
    }else{
        codigo = "-" + codigo;
    }
 
    
    document.getElementById(nombreFormulario).submit();

    
}

function updatetabla(codigo, nombreFormulario) {
    console.log("codigo: " + codigo);
    console.log("codigo.length: " + codigo.length);
    if (codigo.length === 0){
        console.log("codigo vació ");
    }else{
        codigo = "-" + codigo;
    }
    console.log("codigo: " + codigo);
    //Obtención de la información del formulario
    let nombres = document.getElementById("txt-nombres" + codigo).value;
    let apellidoPaterno = document.getElementById("txt-apellido-paterno" + codigo).value;
    let apellidoMaterno = document.getElementById("txt-apellido-materno" + codigo).value;
    let run = document.getElementById("txt-run" +codigo).value;
    let telefono = document.getElementById("txt-telefono" + codigo).value;
    let direccion = document.getElementById("txt-direccion" + codigo).value;
    let email = document.getElementById("txt-email" + codigo).value;
    
    let motivo = document.getElementById("txt-motivo" +codigo).value;
    let mensaje_error = isEmpty(nombres, "NOMBRES");
    //Validación de campos del formulario
    mensaje_error = mensaje_error + isEmpty(apellidoPaterno, "APELLIDO PATERNO");
    mensaje_error = mensaje_error + isEmpty(apellidoMaterno, "APELLIDO MATERNO");
    mensaje_error = mensaje_error + isEmpty(run, "RUN");
    mensaje_error = mensaje_error + isEmpty(telefono, "TELEFONO");
    mensaje_error = mensaje_error + isEmpty(direccion, "DIRECCION");
    mensaje_error = mensaje_error + isEmpty(email, "EMAIL");
    
    mensaje_error = mensaje_error + isEmpty(motivo, "MOTIVO");
    //Mostrar Error
    let msgValidacion = document.getElementById("msg-validacion");
    let msgInformacion = document.getElementById("msg-informacion");
    if (mensaje_error == 0) {
        console.log("DATOS VALIDADOS CORRECTAMENTE.");
        msgValidacion.className = "alert alert-success alert-dismissible fade show";
        msgInformacion.innerHTML = "DATOS VALIDADOS CORRECTAMENTE.";
        document.getElementById(nombreFormulario).submit();
        clearForm(false);
    } else {
        console.log(mensaje_error);
        msgValidacion.className = "alert alert-danger alert-dismissible fade show";
        msgInformacion.innerHTML = mensaje_error.replaceAll("\n", "<br>");
        msgValidacion.hidden = false;
    }
}