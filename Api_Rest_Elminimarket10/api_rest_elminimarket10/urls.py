"""elminimarket10 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from elminimarket10.ws.expose_cliente import client, client_by_codigo
from elminimarket10.ws.expose_despacho import despacho, despacho_by_codigo
from elminimarket10.ws.expose_reclamo import reclamo, reclamo_by_codigo
from elminimarket10.ws.expose_retiro import retiro, retiro_by_codigo
from elminimarket10.ws.expose_sugerencia import sugerencia, sugerencia_by_codigo
from elminimarket10.ws.expose_authentication import login

urlpatterns = [
    path('admin/', admin.site.urls),
    
    path('api/v1/cliente', client, name="client"),
    path('api/v1/cliente/<int:codigo>', client_by_codigo, name="client_by_codigo"),

    path('api/v1/despacho', despacho, name="despacho"), 
    path('api/v1/despacho/<int:codigo>', despacho_by_codigo, name='despacho_by_codigo'),

    path('api/v1/reclamo', reclamo, name="reclamo"), 
    path('api/v1/reclamo/<int:codigo>', reclamo_by_codigo, name='reclamo_by_codigo'),

    path('api/v1/retiro', retiro, name="retiro"), 
    path('api/v1/retiro/<int:codigo>', retiro_by_codigo, name='retiro_by_codigo'),

    path('api/v1/sugerencia', sugerencia, name="sugerencia"), 
    path('api/v1/sugerencia/<int:codigo>', sugerencia_by_codigo, name='sugerencia_by_codigo'),

    path('api/v1/login', login, name='login'), 
]
