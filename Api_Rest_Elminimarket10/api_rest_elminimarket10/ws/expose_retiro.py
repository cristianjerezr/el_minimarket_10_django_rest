from elminimarket10.ws.validate.jsonvalidate import CustomValidate
from django.http import JsonResponse, HttpResponse
from rest_framework.decorators import api_view
from elminimarket10.persistence.models import Cliente, Retiro
import json
from jsonschema import validate
from jsonschema.exceptions import ValidationError
from rest_framework import status
from elminimarket10.ws.util.response_utility import create_response_json, error_authentication
from elminimarket10.ws.util.authorization_utility import validate_authorization

#scheme add client:
scheme_add_client = {
    "type": "object",
    "properties": {
        "motivo":{"type": "string"},
        "email":{"type": "string"},
        "telefono":{"type": "string"},
        "codigo_cliente":{
            "type": "object",
            "properties": {
                "run":{"type": "string"},
                "nombres":{"type": "string"},
                "apellido_paterno":{"type": "string"},
                "apellido_materno":{"type": "string"},
                "direccion" : {"type" : "string"},
            },
        },
    },
    "required": ["motivo", "email", "telefono", "codigo_cliente"],
    "propertiesOrder": ["motivo", "email", "telefono", "codigo_cliente"],
}

@api_view(['POST', 'GET', 'PUT'])
def retiro(request):
    if request.method == 'POST':
        return add_retiro(request)
    if not validate_authorization(request) :
        return error_authentication() 
    if request.method == 'GET':
        return find_all(request)  
    if request.method == 'PUT':
        return update_retiro(request)            

def find_all(request):
    print('method find_all')
    try:
        retiros = Retiro.objects.all().order_by('codigo')

        retiros_json = []
        for retiro in retiros:
            print(retiro.json())
            retiros_json.append(retiro.json())
            
        return JsonResponse(retiros_json, safe=False, content_type="application/json", json_dumps_params={'ensure_ascii': False})
    except Retiro.DoesNotExist as err:
        response = HttpResponse('Error al buscar los retiros')
        response.status_code = status.HTTP_404_NOT_FOUND
        return response             
    except Exception as err:
        print(err)
        response = HttpResponse('Error al conectarse a la base de datos.')
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return response

def add_retiro(request):
    print('method add_cliente')
    retiro = json.loads(request.body.decode("utf-8"))
    print('client -> {0}'.format(retiro))
    try:
        print('validate json')
        validate(instance=retiro, schema=scheme_add_client)
        new_cliente = Cliente(
                            run = retiro.get('codigo_cliente').get('run'),
                            nombres = retiro.get('codigo_cliente').get('nombres'),
                            apellido_paterno = retiro.get('codigo_cliente').get('apellido_paterno'),
                            apellido_materno = retiro.get('codigo_cliente').get('apellido_materno'),
                            direccion = retiro.get('codigo_cliente').get('direccion'),
                    )
        new_cliente.save()
        new_retiro = Retiro(
                            motivo = retiro.get('motivo'),
                            email = retiro.get('email'),
                            telefono = retiro.get('telefono'),
                            codigo_cliente = new_cliente,
                    )
        new_retiro.save()
        return JsonResponse(new_retiro.json(), content_type="application/json", json_dumps_params={'ensure_ascii': False})
    except ValidationError as err:
        print(err)
        response = HttpResponse('Error al validar estructura')
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return response   
    except Exception as err:
        print(err)
        response = HttpResponse('Error al conectarse a la base de datos.')
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return response

def update_retiro(request):
    print('method update_retiro')
    retiro = json.loads(request.body.decode("utf-8"))
    print('client -> {0}'.format(retiro))
    try:
        print('validate json')
        update_retiro = Retiro.objects.get(codigo=retiro.get('codigo'))
        
        print (update_retiro.codigo_cliente.codigo)
        print (retiro.get('codigo_cliente').get('codigo'))
        print (retiro.get('codigo_cliente').get('codigo'))
        if Retiro.objects.get(codigo_cliente = retiro.get('codigo_cliente').get('codigo')):
            print('encontrado')
            update_retiro.motivo = retiro.get('motivo')
            update_retiro.email = retiro.get('email')
            update_retiro.telefono = retiro.get('telefono')
            update_retiro.codigo_cliente.run = retiro.get('codigo_cliente').get('run')
            update_retiro.codigo_cliente.nombres = retiro.get('codigo_cliente').get('nombres')
            update_retiro.codigo_cliente.apellido_paterno = retiro.get('codigo_cliente').get('apellido_paterno')
            update_retiro.codigo_cliente.apellido_materno = retiro.get('codigo_cliente').get('apellido_materno')
            update_retiro.codigo_cliente.direccion = retiro.get('codigo_cliente').get('direccion')
            update_retiro.save(force_update=True)
        else:
            print ('retiro no encontrado')
        return JsonResponse(update_retiro.json(), content_type="application/json", json_dumps_params={'ensure_ascii': False})
    except ValidationError as err:
        print(err)
        response = HttpResponse('Error al validar estructura')
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return response   
    except Exception as err:
        print(err)
        response = HttpResponse('Error al conectarse a la base de datos.')
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return response

@api_view(['GET', 'DELETE'])
def retiro_by_codigo(request, codigo):
    if not validate_authorization(request) :
        return error_authentication()
    if request.method == 'GET':
        return find_by_codigo(request, codigo)
    if request.method == 'DELETE':
        return delete_by_codigo(request, codigo)

def find_by_codigo(request, codigo):
    print('method find_by_codigo -> {0}'.format(codigo))
    try:
        retiro = Retiro.objects.get(codigo=codigo)
        return JsonResponse(retiro.json(), content_type="application/json", json_dumps_params={'ensure_ascii': False})
    except Retiro.DoesNotExist as err:
        return JsonResponse({}, content_type="application/json", json_dumps_params={'ensure_ascii': False}) 
    except Exception as err:
        print(err)
        error_database_json(err, 'Error al buscar cliente por id.')

def delete_by_codigo(request, codigo_cliente):
    print('method delete_by_codigo -> {0}'.format(codigo_cliente))
    try:
       retiro = Retiro.objects.get(codigo_cliente=codigo_cliente)
       retiro.delete()
       cliente = Cliente.objects.get(codigo=codigo_cliente)
       cliente.delete()
       response = HttpResponse('Retiro eliminado.')
       response.status_code = status.HTTP_200_OK
       return response
    except Retiro.DoesNotExist as err:
       response = HttpResponse('Retiro no encontrado. Error al borrar por codigo -> {0}'.format(codigo_cliente))
       response.status_code = status.HTTP_404_NOT_FOUND
       return response
    except Exception as err:
        print(err)
        status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        detail = 'Error al borar el retiro -> {0}'.format(codigo_cliente)
        raise CustomValidate(detail=detail, status_code=status_code)   