from rest_framework.exceptions import APIException
from django.utils.encoding import force_text

class CustomValidate(APIException):

    def __init__(self, detail, field, status_code):
        if status_code is not None:
            self.status_code = status_code
        if detail is not None:
            self.detail = {field: force_text(detail)}

    def __init__(self, detail, status_code):
        if status_code is not None:
            self.status_code = status_code
        if detail is not None:
            self.detail = {detail: force_text(detail)}
