from elminimarket10.ws.validate.jsonvalidate import CustomValidate
from django.http import JsonResponse, HttpResponse
from rest_framework.decorators import api_view
from elminimarket10.persistence.models import Cliente, Reclamo
import json
from jsonschema import validate
from jsonschema.exceptions import ValidationError
from rest_framework import status
from elminimarket10.ws.util.response_utility import create_response_json, error_authentication
from elminimarket10.ws.util.authorization_utility import validate_authorization

#scheme add client:
scheme_add_client = {
    "type": "object",
    "properties": {
        "motivo":{"type": "string"},
        "email":{"type": "string"},
        "telefono":{"type": "string"},
        "codigo_cliente":{
            "type": "object",
            "properties": {
                "run":{"type": "string"},
                "nombres":{"type": "string"},
                "apellido_paterno":{"type": "string"},
                "apellido_materno":{"type": "string"},
                "direccion" : {"type" : "string"},
            },
        },
    },
    "required": ["motivo", "email", "telefono", "codigo_cliente"],
    "propertiesOrder": ["motivo", "email", "telefono", "codigo_cliente"],
}

@api_view(['POST', 'GET', 'PUT'])
def reclamo(request):
    if request.method == 'POST':
        return add_reclamo(request)
    if not validate_authorization(request) :
        return error_authentication() 
    if request.method == 'GET':
        return find_all(request)        
    if request.method == 'PUT':
        return update_reclamo(request)  

def find_all(request):
    print('method find_all')
    try:
        reclamos = Reclamo.objects.all().order_by('codigo')

        reclamos_json = []
        for reclamo in reclamos:
            print(reclamo.json())
            reclamos_json.append(reclamo.json())
            
        return JsonResponse(reclamos_json, safe=False, content_type="application/json", json_dumps_params={'ensure_ascii': False})
    except Reclamo.DoesNotExist as err:
        response = HttpResponse('Error al buscar los reclamos')
        response.status_code = status.HTTP_404_NOT_FOUND
        return response             
    except Exception as err:
        print(err)
        response = HttpResponse('Error al conectarse a la base de datos.')
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return response

def add_reclamo(request):
    print('method add_cliente')
    reclamo = json.loads(request.body.decode("utf-8"))
    print('client -> {0}'.format(reclamo))
    try:
        print('validate json')
        validate(instance=reclamo, schema=scheme_add_client)
        new_cliente = Cliente(
                            run = reclamo.get('codigo_cliente').get('run'),
                            nombres = reclamo.get('codigo_cliente').get('nombres'),
                            apellido_paterno = reclamo.get('codigo_cliente').get('apellido_paterno'),
                            apellido_materno = reclamo.get('codigo_cliente').get('apellido_materno'),
                            direccion = reclamo.get('codigo_cliente').get('direccion'),
                    )
        new_cliente.save()
        new_reclamo = Reclamo(
                            motivo = reclamo.get('motivo'),
                            email = reclamo.get('email'),
                            telefono = reclamo.get('telefono'),
                            codigo_cliente = new_cliente,
                    )
        new_reclamo.save()
        return JsonResponse(new_reclamo.json(), content_type="application/json", json_dumps_params={'ensure_ascii': False})
    except ValidationError as err:
        print(err)
        response = HttpResponse('Error al validar estructura')
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return response   
    except Exception as err:
        print(err)
        response = HttpResponse('Error al conectarse a la base de datos.')
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return response

def update_reclamo(request):
    print('method update_reclamo')
    reclamo = json.loads(request.body.decode("utf-8"))
    print('client -> {0}'.format(reclamo))
    try:
        print('validate json')
        update_reclamo = Reclamo.objects.get(codigo=reclamo.get('codigo'))
        
        print (update_reclamo.codigo_cliente.codigo)
        print (reclamo.get('codigo_cliente').get('codigo'))
        print (reclamo.get('codigo_cliente').get('codigo'))
        if Reclamo.objects.get(codigo_cliente = reclamo.get('codigo_cliente').get('codigo')):
            print('encontrado')
            update_reclamo.motivo = reclamo.get('motivo')
            update_reclamo.email = reclamo.get('email')
            update_reclamo.telefono = reclamo.get('telefono')
            update_reclamo.codigo_cliente.run = reclamo.get('codigo_cliente').get('run')
            update_reclamo.codigo_cliente.nombres = reclamo.get('codigo_cliente').get('nombres')
            update_reclamo.codigo_cliente.apellido_paterno = reclamo.get('codigo_cliente').get('apellido_paterno')
            update_reclamo.codigo_cliente.apellido_materno = reclamo.get('codigo_cliente').get('apellido_materno')
            update_reclamo.codigo_cliente.direccion = reclamo.get('codigo_cliente').get('direccion')
            update_reclamo.save(force_update=True)
        else:
            print ('reclamo no encontrado')
        return JsonResponse(update_reclamo.json(), content_type="application/json", json_dumps_params={'ensure_ascii': False})
    except ValidationError as err:
        print(err)
        response = HttpResponse('Error al validar estructura')
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return response   
    except Exception as err:
        print(err)
        response = HttpResponse('Error al conectarse a la base de datos.')
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return response

@api_view(['GET', 'DELETE'])
def reclamo_by_codigo(request, codigo):
    if not validate_authorization(request) :
        return error_authentication()
    if request.method == 'GET':
        return find_by_codigo(request, codigo)
    if request.method == 'DELETE':
        return delete_by_codigo(request, codigo)

def find_by_codigo(request, codigo):
    print('method find_by_codigo -> {0}'.format(codigo))
    try:
        reclamo = Reclamo.objects.get(codigo=codigo)
        return JsonResponse(reclamo.json(), content_type="application/json", json_dumps_params={'ensure_ascii': False})
    except Reclamo.DoesNotExist as err:
        return JsonResponse({}, content_type="application/json", json_dumps_params={'ensure_ascii': False}) 
    except Exception as err:
        print(err)
        error_database_json(err, 'Error al buscar cliente por id.')

def delete_by_codigo(request, codigo_cliente):
    print('method delete_by_codigo -> {0}'.format(codigo_cliente))
    try:
       reclamo = Reclamo.objects.get(codigo_cliente=codigo_cliente)
       reclamo.delete()
       cliente = Cliente.objects.get(codigo=codigo_cliente)
       cliente.delete()
       response = HttpResponse('Reclamo eliminado.')
       response.status_code = status.HTTP_200_OK
       return response
    except Reclamo.DoesNotExist as err:
       response = HttpResponse('Reclamo no encontrado. Error al borrar por codigo -> {0}'.format(codigo_cliente))
       response.status_code = status.HTTP_404_NOT_FOUND
       return response
    except Exception as err:
        print(err)
        status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        detail = 'Error al borar el reclamo -> {0}'.format(codigo_cliente)
        raise CustomValidate(detail=detail, status_code=status_code)   