from django.http import JsonResponse, HttpResponse
from rest_framework.decorators import api_view
import json
from jsonschema import validate
from jsonschema.exceptions import ValidationError
from rest_framework import status
from django.contrib.auth.models import User
from django.contrib.auth.hashers import check_password
from rest_framework.authtoken.models import Token
from django.db.utils import IntegrityError
from elminimarket10.ws.util.response_utility import create_response_json, error_authentication

def create_token(username, password):
    print('create_token')
    try:
        user = User.objects.get(username=username)
        if check_password(password, user.password) :
            try:
                token = Token.objects.create(user=user)
                print('token -> {0}'.format(token))
                return create_response_json(
                    {   'token': token.key,
                        'created': token.created
                    }
                )
            except IntegrityError as e:
                token = Token.objects.get(user=user)
                token.delete()
                token = Token.objects.create(user=user)
                return create_response_json(
                    {   'token': token.key,
                        'created': token.created
                    }
                )
        else:
            return error_authentication()    
    except User.DoesNotExist as e:
        print('Error {0}'.format(e))
        return error_authentication()


@api_view(['POST'])
def login(request):
    print('login')

    try:
        user_pass = json.loads(request.body.decode("utf-8"))    
        print('user -> {0}'.format(user_pass))
        username = user_pass.get('username')
        password = user_pass.get('pass')
        return create_token(username, password)                      
    except Exception as e:
        print('Error {0}'.format(e))
        return error_authentication()

