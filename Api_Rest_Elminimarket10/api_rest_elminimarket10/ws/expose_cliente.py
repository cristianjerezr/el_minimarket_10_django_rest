from elminimarket10.ws.validate.jsonvalidate import CustomValidate
from django.http import JsonResponse, HttpResponse
from rest_framework.decorators import api_view
from elminimarket10.persistence.models import Cliente
import json
from jsonschema import validate
from jsonschema.exceptions import ValidationError
from rest_framework import status

#scheme add client:
scheme_add_client ={
    "type" : "object",
    "properties" : {
        "run" : {"type" : "string"},
        "nombres" : {"type" : "string"},
        "apellido_paterno" : {"type" : "string"},
        "apellido_materno" : {"type" : "string"},
        "direccion" : {"type" : "string"},
    },
    "required": [ "run", "nombres", "apellido_paterno", "apellido_materno", "direccion"],
    "propertiesOrder": ["run", "nombres", "apellido_paterno", "apellido_materno", "direccion"],
}

#schem update client:
scheme_update_client ={
    "type" : "object",
    "properties" : {
        "codigo" : {"type" : "integer"},
        "run" : {"type" : "string"},
        "nombres" : {"type" : "string"},
        "apellido_paterno" : {"type" : "string"},
        "apellido_materno" : {"type" : "string"},
        "direccion" : {"type" : "string"},
    },
    "required": ["codigo", "run", "nombres", "apellido_paterno", "apellido_materno", "direccion"],
    "propertiesOrder": ["codigo", "run", "nombres", "apellido_paterno", "apellido_materno", "direccion"],
}


def invalidate_json(err, schema): 
    print(err)
    print('invalidate json')
    status_code = status.HTTP_409_CONFLICT
    detail = 'Estructura incorrecta, el esquema esperado es -> {0}'.format(schema) 
    raise CustomValidate(detail=detail, status_code=status_code)  

def error_database_json(err, detail):
    print(err)
    print('database error')
    status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
    raise CustomValidate(detail=detail, status_code=status_code)

@api_view(['POST', 'GET', 'PUT'])
def client(request):

    if request.method == 'POST':
        return add_client(request)
    if request.method == 'GET':
        return find_all(request)
    if request.method == 'PUT':
        return update_client(request)       
        
def add_client(request):
    print('method add_cliente')
    client = json.loads(request.body.decode("utf-8"))
    print('client -> {0}'.format(client))
    try:
        print('validate json')
        validate(instance=client, schema=scheme_add_client)
        new_client = Cliente(
                            run = client.get('run'),
                            nombres = client.get('nombres'),
                            apellido_paterno = client.get('apellido_paterno'),
                            apellido_materno = client.get('apellido_materno'),
                            direccion = client.get('direccion'),
                    )
        new_client.save()
        return JsonResponse(new_client.json(), content_type="application/json", json_dumps_params={'ensure_ascii': False})
    except ValidationError as err:
        print(err)
        invalidate_json(err, scheme_add_client)
    except Exception as err:
        print(err)
        error_database_json(err, 'Error al crear el cliente.')


def find_all(request):
    print('method find all')
    try:
        clients = Cliente.objects.all().order_by('codigo').values()
        return JsonResponse(list(clients), safe=False, content_type="application/json", json_dumps_params={'ensure_ascii': False})    
    except Exception as err:
        print(err)
        error_database_json(err, 'Error al buscar los clientes.')


def update_client(request):
    print('method update_client')
    client = json.loads(request.body.decode("utf-8"))
    print('client -> {0}'.format(client))
    try:
        print('validate json')
        validate(instance=client, schema=scheme_update_client)
        update_client = Cliente.objects.get(codigo=client.get("codigo"))
        update_client.codigo = client.get("codigo")
        update_client.run = client.get("run")
        update_client.codigo = client.get("codigo")
        update_client.nombres = client.get("nombres")
        update_client.apellido_paterno = client.get("apellido_paterno")
        update_client.apellido_materno = client.get("apellido_materno")
        update_client.direccion = client.get("direccion")
        update_client.save(force_update=True)
        return JsonResponse(update_client.json(), content_type="application/json", json_dumps_params={'ensure_ascii': False})
    except ValidationError as err:
        invalidate_json(err, scheme_update_client)
    except Cliente.DoesNotExist as err:
        error_database_json(err, "Cliente no existe.")        
    except Exception as err:
        print(err)
        error_database_json(err, 'Error al actualizar el cliente.')

@api_view(['GET', 'DELETE'])
def client_by_codigo(request, codigo):
    if request.method == 'GET':
        return find_by_codigo(request, codigo)
    if request.method == 'DELETE':
        return delete_by_codigo(request, codigo)

def find_by_codigo(request, codigo):
    print('method find_by_codigo -> {0}'.format(codigo))
    try:
        client = Cliente.objects.get(codigo=codigo)
        return JsonResponse(client.json(), content_type="application/json", json_dumps_params={'ensure_ascii': False})
    except Cliente.DoesNotExist as err:
        #error_database_json(err, "Cliente no existe.")
        return JsonResponse({}, content_type="application/json", json_dumps_params={'ensure_ascii': False}) 
    except Exception as err:
        print(err)
        error_database_json(err, 'Error al buscar cliente por id.')

def delete_by_codigo(request, codigo):
    print('method delete_by_codigo cliente-> {0}'.format(codigo))
    try:
       client = Cliente.objects.get(codigo=codigo)
       client.delete()
       response = HttpResponse('Cliente eliminado.')
       response.status_code = status.HTTP_200_OK
       return response
    except Cliente.DoesNotExist as err:
       response = HttpResponse('Cliente no encontrado. Error al borrar por codigo -> {0}'.format(codigo))
       response.status_code = status.HTTP_404_NOT_FOUND
       return response
    except Exception as err:
        print(err)
        status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        detail = 'Error al borar el cliente -> {0}'.format(codigo)
        raise CustomValidate(detail=detail, status_code=status_code)









