from rest_framework.authtoken.models import Token
from elminimarket10.ws.util.response_utility import error_authentication



def validate_authorization(request):

    authorization = request.headers.get('Authorization')
    print('authorization: {0}'.format(authorization))

    if "Token" in authorization :
        token = authorization.replace('Token ', '')
        print('search token')
        try:   
            token = Token.objects.get(key=token) 
            return True           
        except Token.DoesNotExist as e:
            print('Error {0}'.format(e))
            return False
    else:
        return False   