from django.http import JsonResponse, HttpResponse
from rest_framework import status

def create_response_content_type(message, status, content_type):
    print(message)
    response = HttpResponse(message, content_type=content_type)
    response.status_code = status
    return response

def create_reponse(message, status):
    return create_response_content_type(message, status, 'text/plain')

def create_response_json(json_obj):
    return JsonResponse(json_obj, content_type='application/json',
    json_dumps_params={'ensure_ascii':False})

def error_authentication():
    return create_reponse('Usuario o Contraseña inválida.', 
        status.HTTP_401_UNAUTHORIZED)       