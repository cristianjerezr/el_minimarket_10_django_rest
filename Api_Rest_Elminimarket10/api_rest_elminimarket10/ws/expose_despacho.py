from elminimarket10.ws.validate.jsonvalidate import CustomValidate
from django.http import JsonResponse, HttpResponse
from rest_framework.decorators import api_view
from elminimarket10.persistence.models import Cliente, Despacho
import json
from jsonschema import validate
from jsonschema.exceptions import ValidationError
from rest_framework import status
from elminimarket10.ws.util.response_utility import create_response_json, error_authentication
from elminimarket10.ws.util.authorization_utility import validate_authorization

#scheme add client:
scheme_add_client = {
    "type": "object",
    "properties": {
        "motivo":{"type": "string"},
        "email":{"type": "string"},
        "telefono":{"type": "string"},
        "codigo_cliente":{
            "type": "object",
            "properties": {
                "run":{"type": "string"},
                "nombres":{"type": "string"},
                "apellido_paterno":{"type": "string"},
                "apellido_materno":{"type": "string"},
                "direccion" : {"type" : "string"},
            },
        },
    },
    "required": ["motivo", "email", "telefono", "codigo_cliente"],
    "propertiesOrder": ["motivo", "email", "telefono", "codigo_cliente"],
}

@api_view(['POST', 'GET', 'PUT'])
def despacho(request):
    if request.method == 'POST':
        return add_despacho(request) 
    if not validate_authorization(request) :
        return error_authentication()  
    if request.method == 'GET':
        return find_all_despachos(request)       
    if request.method == 'PUT':
        return update_despacho(request)


def find_all_despachos(request):
    print('method find_all_despachos')
    try:
        despachos = Despacho.objects.all().order_by('codigo')

        despachos_json = []
        for despacho in despachos:
            print(despacho.json())
            despachos_json.append(despacho.json())
            
        return JsonResponse(despachos_json, safe=False, content_type="application/json", json_dumps_params={'ensure_ascii': False})
    except Despacho.DoesNotExist as err:
        response = HttpResponse('Error al buscar los despachos')
        response.status_code = status.HTTP_404_NOT_FOUND
        return response             
    except Exception as err:
        print(err)
        response = HttpResponse('Error al conectarse a la base de datos.')
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return response

def add_despacho(request):
    print('method add_cliente')
    despacho = json.loads(request.body.decode("utf-8"))
    print('client -> {0}'.format(despacho))
    try:
        print('validate json')
        validate(instance=despacho, schema=scheme_add_client)
        new_cliente = Cliente(
                            run = despacho.get('codigo_cliente').get('run'),
                            nombres = despacho.get('codigo_cliente').get('nombres'),
                            apellido_paterno = despacho.get('codigo_cliente').get('apellido_paterno'),
                            apellido_materno = despacho.get('codigo_cliente').get('apellido_materno'),
                            direccion = despacho.get('codigo_cliente').get('direccion'),
                    )
        new_cliente.save()
        new_despacho = Despacho(
                            motivo = despacho.get('motivo'),
                            email = despacho.get('email'),
                            telefono = despacho.get('telefono'),
                            codigo_cliente = new_cliente,
                    )
        new_despacho.save()
        return JsonResponse(new_despacho.json(), content_type="application/json", json_dumps_params={'ensure_ascii': False})
    except ValidationError as err:
        print(err)
        response = HttpResponse('Error al validar estructura')
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return response   
    except Exception as err:
        print(err)
        response = HttpResponse('Error al conectarse a la base de datos.')
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return response

def update_despacho(request):
    print('method update_despacho')
    ##print ('despacho ->{0}'.format(codigo_cliente))
    despacho = json.loads(request.body.decode("utf-8"))
    print('client -> {0}'.format(despacho))
    try:
        print('validate json')
        update_despacho = Despacho.objects.get(codigo=despacho.get('codigo'))
        
        print (update_despacho.codigo_cliente.codigo)
        print (despacho.get('codigo_cliente').get('codigo'))
        print (despacho.get('codigo_cliente').get('codigo'))
        if Despacho.objects.get(codigo_cliente = despacho.get('codigo_cliente').get('codigo')):
            print('encontrado')
            update_despacho.motivo = despacho.get('motivo')
            update_despacho.email = despacho.get('email')
            update_despacho.telefono = despacho.get('telefono')
            update_despacho.codigo_cliente.run = despacho.get('codigo_cliente').get('run')
            update_despacho.codigo_cliente.nombres = despacho.get('codigo_cliente').get('nombres')
            update_despacho.codigo_cliente.apellido_paterno = despacho.get('codigo_cliente').get('apellido_paterno')
            update_despacho.codigo_cliente.apellido_materno = despacho.get('codigo_cliente').get('apellido_materno')
            update_despacho.codigo_cliente.direccion = despacho.get('codigo_cliente').get('direccion')
            update_despacho.save(force_update=True)
        else:
            print ('despacho no encontrado')
        return JsonResponse(update_despacho.json(), content_type="application/json", json_dumps_params={'ensure_ascii': False})
    except ValidationError as err:
        print(err)
        response = HttpResponse('Error al validar estructura')
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return response   
    except Exception as err:
        print(err)
        response = HttpResponse('Error al conectarse a la base de datos.')
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return response        

@api_view(['GET', 'DELETE'])
def despacho_by_codigo(request, codigo):
    if not validate_authorization(request) :
        return error_authentication() 
    if request.method == 'GET':
        return find_by_codigo(request, codigo)
    if request.method == 'DELETE':
        return delete_by_codigo(request, codigo)

def find_by_codigo(request, codigo):
    print('method find_by_codigo -> {0}'.format(codigo))
    try:
        despacho = Despacho.objects.get(codigo=codigo)
        return JsonResponse(despacho.json(), content_type="application/json", json_dumps_params={'ensure_ascii': False})
    except Despacho.DoesNotExist as err:
        return JsonResponse({}, content_type="application/json", json_dumps_params={'ensure_ascii': False}) 
    except Exception as err:
        print(err)
        error_database_json(err, 'Error al buscar cliente por id.')

def delete_by_codigo(request, codigo_cliente):
    print('method delete_by_codigo despacho -> {0}'.format(codigo_cliente))
    try:
       despacho = Despacho.objects.get(codigo_cliente=codigo_cliente)
       despacho.delete()
       cliente = Cliente.objects.get(codigo=codigo_cliente)
       cliente.delete()
       response = HttpResponse('Despacho eliminado.')
       response.status_code = status.HTTP_200_OK
       return response
    except Despacho.DoesNotExist as err:
       response = HttpResponse('Despacho no encontrado. Error al borrar por codigo -> {0}'.format(codigo_cliente))
       response.status_code = status.HTTP_404_NOT_FOUND
       return response
    except Exception as err:
        print(err)
        status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        detail = 'Error al borar el despacho -> {0}'.format(codigo_cliente)
        raise CustomValidate(detail=detail, status_code=status_code)   