from elminimarket10.ws.validate.jsonvalidate import CustomValidate
from django.http import JsonResponse, HttpResponse
from rest_framework.decorators import api_view
from elminimarket10.persistence.models import Cliente, Sugerencia
import json
from jsonschema import validate
from jsonschema.exceptions import ValidationError
from rest_framework import status
from elminimarket10.ws.util.response_utility import create_response_json, error_authentication
from elminimarket10.ws.util.authorization_utility import validate_authorization

#scheme add client:
scheme_add_client = {
    "type": "object",
    "properties": {
        "motivo":{"type": "string"},
        "email":{"type": "string"},
        "telefono":{"type": "string"},
        "codigo_cliente":{
            "type": "object",
            "properties": {
                "run":{"type": "string"},
                "nombres":{"type": "string"},
                "apellido_paterno":{"type": "string"},
                "apellido_materno":{"type": "string"},
                "direccion" : {"type" : "string"},
            },
        },
    },
    "required": ["motivo", "email", "telefono", "codigo_cliente"],
    "propertiesOrder": ["motivo", "email", "telefono", "codigo_cliente"],
}

@api_view(['POST', 'GET', 'PUT'])
def sugerencia(request):
    if request.method == 'POST':
        return add_sugerencia(request) 
    if not validate_authorization(request) :
        return error_authentication() 
    if request.method == 'GET':
        return find_all(request)        
    if request.method == 'PUT':
        return update_sugerencia(request)  

def find_all(request):
    print('method find_all')
    try:
        sugerencias = Sugerencia.objects.all().order_by('codigo')

        sugerencias_json = []
        for sugerencia in sugerencias:
            print(sugerencia.json())
            sugerencias_json.append(sugerencia.json())
            
        return JsonResponse(sugerencias_json, safe=False, content_type="application/json", json_dumps_params={'ensure_ascii': False})
    except Sugerencia.DoesNotExist as err:
        response = HttpResponse('Error al buscar los sugerencias')
        response.status_code = status.HTTP_404_NOT_FOUND
        return response             
    except Exception as err:
        print(err)
        response = HttpResponse('Error al conectarse a la base de datos.')
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return response

def add_sugerencia(request):
    print('method add_cliente')
    sugerencia = json.loads(request.body.decode("utf-8"))
    print('client -> {0}'.format(sugerencia))
    try:
        print('validate json')
        validate(instance=sugerencia, schema=scheme_add_client)
        new_cliente = Cliente(
                            run = sugerencia.get('codigo_cliente').get('run'),
                            nombres = sugerencia.get('codigo_cliente').get('nombres'),
                            apellido_paterno = sugerencia.get('codigo_cliente').get('apellido_paterno'),
                            apellido_materno = sugerencia.get('codigo_cliente').get('apellido_materno'),
                            direccion = sugerencia.get('codigo_cliente').get('direccion'),
                    )
        new_cliente.save()
        new_sugerencia = Sugerencia(
                            motivo = sugerencia.get('motivo'),
                            email = sugerencia.get('email'),
                            telefono = sugerencia.get('telefono'),
                            codigo_cliente = new_cliente,
                    )
        new_sugerencia.save()
        return JsonResponse(new_sugerencia.json(), content_type="application/json", json_dumps_params={'ensure_ascii': False})
    except ValidationError as err:
        print(err)
        response = HttpResponse('Error al validar estructura')
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return response   
    except Exception as err:
        print(err)
        response = HttpResponse('Error al conectarse a la base de datos.')
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return response

def update_sugerencia(request):
    print('method update_sugerencia')
    sugerencia = json.loads(request.body.decode("utf-8"))
    print('client -> {0}'.format(sugerencia))
    try:
        print('validate json')
        update_sugerencia = Sugerencia.objects.get(codigo=sugerencia.get('codigo'))
        
        print (update_sugerencia.codigo_cliente.codigo)
        print (sugerencia.get('codigo_cliente').get('codigo'))
        print (sugerencia.get('codigo_cliente').get('codigo'))
        if Sugerencia.objects.get(codigo_cliente = sugerencia.get('codigo_cliente').get('codigo')):
            print('encontrado')
            update_sugerencia.motivo = sugerencia.get('motivo')
            update_sugerencia.email = sugerencia.get('email')
            update_sugerencia.telefono = sugerencia.get('telefono')
            update_sugerencia.codigo_cliente.run = sugerencia.get('codigo_cliente').get('run')
            update_sugerencia.codigo_cliente.nombres = sugerencia.get('codigo_cliente').get('nombres')
            update_sugerencia.codigo_cliente.apellido_paterno = sugerencia.get('codigo_cliente').get('apellido_paterno')
            update_sugerencia.codigo_cliente.apellido_materno = sugerencia.get('codigo_cliente').get('apellido_materno')
            update_sugerencia.codigo_cliente.direccion = sugerencia.get('codigo_cliente').get('direccion')
            update_sugerencia.save(force_update=True)
        else:
            print ('sugerencia no encontrado')
        return JsonResponse(update_sugerencia.json(), content_type="application/json", json_dumps_params={'ensure_ascii': False})
    except ValidationError as err:
        print(err)
        response = HttpResponse('Error al validar estructura')
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return response   
    except Exception as err:
        print(err)
        response = HttpResponse('Error al conectarse a la base de datos.')
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return response

@api_view(['GET', 'DELETE'])
def sugerencia_by_codigo(request, codigo):
    if not validate_authorization(request) :
        return error_authentication()
    if request.method == 'GET':
        return find_by_codigo(request, codigo)
    if request.method == 'DELETE':
        return delete_by_codigo(request, codigo)

def find_by_codigo(request, codigo):
    print('method find_by_codigo -> {0}'.format(codigo))
    try:
        sugerencia = Sugerencia.objects.get(codigo=codigo)
        return JsonResponse(sugerencia.json(), content_type="application/json", json_dumps_params={'ensure_ascii': False})
    except Sugerencia.DoesNotExist as err:
        return JsonResponse({}, content_type="application/json", json_dumps_params={'ensure_ascii': False}) 
    except Exception as err:
        print(err)
        error_database_json(err, 'Error al buscar cliente por id.')

def delete_by_codigo(request, codigo_cliente):
    print('method delete_by_codigo -> {0}'.format(codigo_cliente))
    try:
       sugerencia = Sugerencia.objects.get(codigo_cliente=codigo_cliente)
       sugerencia.delete()
       cliente = Cliente.objects.get(codigo=codigo_cliente)
       cliente.delete()
       response = HttpResponse('Sugerencia eliminado.')
       response.status_code = status.HTTP_200_OK
       return response
    except Sugerencia.DoesNotExist as err:
       response = HttpResponse('Sugerencia no encontrado. Error al borrar por codigo -> {0}'.format(codigo_cliente))
       response.status_code = status.HTTP_404_NOT_FOUND
       return response
    except Exception as err:
        print(err)
        status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        detail = 'Error al borar el sugerencia -> {0}'.format(codigo_cliente)
        raise CustomValidate(detail=detail, status_code=status_code)   